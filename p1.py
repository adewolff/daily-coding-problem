"""
Given a list of numbers and a number k, return whether any two numbers from the list add up to k.

For example, given [10, 15, 3, 7] and k of 17, return true since 10 + 7 is 17.

Bonus: Can you do this in one pass?
"""

# 1 pass:
from collections import Counter

input_list = [10, 15, 3, 7]
k = 17


def is_sum(li, goal):
    counted = Counter()

    for i in range(len(li)):
        if counted.__contains__(goal - li[i]):
            return True
        counted[li[i]] += 1
    return False


final = is_sum(input_list, k)
print(final)
