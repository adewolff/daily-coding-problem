"""
Given an array of integers, return a new array such that each element at index i of the new array is the product of all the numbers in the original array except the one at i.

For example, if our input was [1, 2, 3, 4, 5], the expected output would be [120, 60, 40, 30, 24]. If our input was [3, 2, 1], the expected output would be [2, 3, 6].

Follow-up: what if you can't use division?
"""

# No division:
from functools import reduce

inp_list = [1, 2, 3, 4, 5]


def multiplier(li):
    res = []

    for i in range(len(li)):
        minus_list = li.copy()
        del minus_list[i]

        prod = reduce((lambda x, y: x * y), minus_list)
        res.append(prod)

    return res


answer = multiplier(inp_list)
print(answer)

